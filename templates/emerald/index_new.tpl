<!-- BEGIN TEMPLATE index_new.tpl -->
<table cellpadding="1" cellspacing="0" class="list">
    {VAR first_pass 1}
    {LOOP FORUMS}
        {IF FORUMS->level 0}
            <tr>
                {IF FORUMS->forum_id FORUMS->vroot}
                    <th align="left">
                        <img src="{URL->TEMPLATE}/images/folder.png" class="icon1616" alt="&bull;" />
                        {LANG->Forums}
                        {IF FORUMS->description}
                            <small>{FORUMS->description}</small>
                        {/IF}
                    </th>
                {ELSE}
                    <th align="left">
                        <img src="{URL->TEMPLATE}/images/folder.png" class="icon1616" alt="&bull;" />
                        <a href="{FORUMS->URL->LIST}">{FORUMS->name}</a>
                    </th>
                {/IF}
                <th>{LANG->Threads}</th>
                <th>{LANG->Posts}</th>
                <th align="left">{LANG->LastPost}</th>
            </tr>
        {ELSE}
            <tr>
                {IF FORUMS->folder_flag}
                    <td colspan="4">
                        <img src="{URL->TEMPLATE}/images/folder.png" class="icon1616" alt="&bull;" />
                        <a href="{FORUMS->URL->INDEX}">{FORUMS->name}</a><br>{FORUMS->description}
                    </td>
                {ELSE}
                    <td width="55%">
                        <B><a href="{FORUMS->URL->LIST}">{FORUMS->name}</a>{IF FORUMS->new_message_check}&nbsp;&nbsp;<span class="new-indicator">({LANG->NewMessages})</span>{/IF}</B>
                        <br />{FORUMS->description}                       
                        {IF FORUMS->URL->FEED}<a class="icon icon-feed" href="{FORUMS->URL->FEED}">{FEED}</a>{/IF}
                    </td>
                    <td align="center" width="12%" nowrap="nowrap">
                        {FORUMS->thread_count}
                        {IF FORUMS->new_threads}
                            (<span class="new-flag">{FORUMS->new_threads} {LANG->newflag}</span>)
                        {/IF}
                    </td>
                    <td align="center" width="12%" nowrap="nowrap">
                        {FORUMS->message_count}
                        {IF FORUMS->new_messages}
                            (<span class="new-flag">{FORUMS->new_messages} {LANG->newflag}</span>)
                        {/IF}
                    </td>
                    <td width="21%" nowrap="nowrap">
                        {FORUMS->last_post}
                    </td>
                {/IF}
            </tr>
        {/IF}
    {/LOOP FORUMS}
</table>
<!-- END TEMPLATE index_new.tpl -->
