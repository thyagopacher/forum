<?php
/**
 * Constantes para tipo de CORPO DA MENSAGEM.
 */
define("cTipoCorpoMensagem_Texto", 0);
define("cTipoCorpoMensagem_HTML", 1);

class CEmail{
	/**
	 * Servidor de e-mail. Array associativo com: nome & endere�o
	 */
	var $Host;
	
	/**
	 * Porta de conex�o para envio de e-mail. Padr�o 25.
	 */
	var $Porta;
	
	/**
	 * Se � para autenticar ou n�o
	 */
	var $autenticar;
	
	/**
	 * Usu�rio para autentica��o
	 */
	var $Usuario;
	
	/**
	 * Senha para autentica��o
	 */
	var $Senha;
	
	/**
	 * E-mail FROM. Array associativo com endere�o e nome.
	 * ['nome'] = "Teste"
	 * ['endereco'] = "teste@blueone.com.br"
	 */
	var $From;
	
	/**
	 * Array de e-mails associativo, com endere�o e nome, para "REPLY TO" 
	 * ['nome'] = "Teste"
	 * ['endereco'] = "teste@blueone.com.br"
	 */	
	var $ReplyTo;
	
	/**
	 * E-mail para confirma��o de leitura.
	 */
	var $ConfirmReadingTo;
	
	/**
	 * Assunto do e-mail.
	 */
	var $assunto;
	
	/**
	 * Mensagem do e-mail.
	 */
	var $conteudo;
	
	/**
	 * Tipo do conte�do do e-mail. Assume as constantes: cTipoCorpoMensagem_Texto / cTipoCorpoMensagem_HTML
	 */
	var $tipoConteudo;
	
	/**
	 * Array de e-mails associativo, com endere�o e nome, para "TO" 
	 * ['nome'] = "Teste"
	 * ['endereco'] = "teste@blueone.com.br"
	 */
	var $To;
	
	/**
	 * Array de e-mails associativo, com endere�o e nome, para "CC" 
	 * ['nome'] = "Teste"
	 * ['endereco'] = "teste@blueone.com.br"
	 */
	var $Cc;
	
	/**
	 * Array de e-mails associativo, com endere�o e nome, para "BCC" (c�pia oculta) 
	 * ['nome'] = "Teste"
	 * ['endereco'] = "teste@blueone.com.br"
	 */
	var $Bcc;
	
	/**
	 * Anexos. Caminhos dos arquivos que ser�o anexados nas mensagens.
	 */
	var $anexos;
	
	function CEmail(){
		$this->Host = array('nome' => '', 'endereco' => '');
		$this->autenticar = false;
		//$this->Porta = 25;
		$this->Porta = 587;
		$this->Usuario = '';
		$this->Senha = '';
		$this->From = array('nome' => '', 'endereco' => '');
		$this->ConfirmReadingTo = '';
		$this->ReplyTo = array();
		$this->To = array();
		$this->Cc = array();
		$this->Bcc = array();
		$this->assunto = '';
		$this->conteudo = '';
		$this->tipoConteudo = cTipoCorpoMensagem_Texto;
		$this->anexos = array();					
	}
}

/**
 * Envia um e-mail utilizando a biblioteca Pear::Mail 
 * @param CEmail $aEmail Objeto com as informa��es do e-mail para serem enviadas.
 * @param String $erros Array que receber� as mensagens de erros (se o envio n�o for bem sucedido).
 * @return Boolean True: Enviou. False: N�o enviou.
 */
function enviarEmail_PearMail($aEmail){
	// Estes requires s�o do PEAR, s�o arquivos que n�o est�o nas pastas do bemtevi.
	require_once("Mail/mime.php");
	require_once("Mail.php");
	
	$headersAux = array('From' => $aEmail->From['endereco'],
					 	'Subject' => $aEmail->assunto,
					 	);
	/*	
	 * 
	 * MAIL do PEAR n�o trata corretamente c�pia cortesia oculta
	 * para isto basta adicionar os endere�os que dever�o ser ocultos no $recipients 			 	
	$countAux = count($aEmail->Bcc);
	if ($countAux){
		$headersAux['Bcc'] = $aEmail->Bcc[0]['endereco'];
		for ($j=1; $j < $countAux; $j++) {
			$headersAux['Bcc'] .= ', ' . $aEmail->Bcc[$j]['endereco']; 
		}	
	}	*/		
	
	$countAux = count($aEmail->ReplyTo);
	if ($countAux){
		$headersAux['Reply-To'] = $aEmail->ReplyTo[0]['endereco'];
		for ($j=1; $j < $countAux; $j++) {
			$headersAux['Reply-To'] .= ', ' . $aEmail->ReplyTo[$j]['endereco'];
		}
	}		 	
	
	$mime = new Mail_mime();
	if ($aEmail->tipoConteudo == cTipoCorpoMensagem_HTML){
		$mime->setHTMLBody($aEmail->conteudo);
	} else {
		$mime->setTXTBody($aEmail->conteudo);
	}

	for ($j=0; $j < count($aEmail->anexos); $j++) {
		$mime->AddAttachment($aEmail->anexos[$j]);
	}
	
	$body = $mime->get();
	$headers = $mime->headers($headersAux);
	$mail = Mail::factory('smtp',
  						  array ('host' => $aEmail->Host['endereco'],
  						  		 'port' => $aEmail->Porta, 
								 'debug'=> false, 
								 'auth' => $aEmail->autenticar, 
								 'username' => $aEmail->Usuario,
								 'password' => $aEmail->Senha)
						 );
	$countAux = count($aEmail->To);
	if ($countAux){
		$adress = $aEmail->To[0]['endereco'];
		for ($j=1; $j < $countAux; $j++) {
			$adress .= ', ' . $aEmail->To[$j]['endereco']; 
		}	
	}
	/**
	 * Os e-mails dos clientes que devem ser OCULTOS s�o adicionados aqui porque a biblioteca n�o trata BCC
	 * Por�m a partir do segundo e-mail no TO, os demais (inclusive o segundo) s�o OCULTOS.
	 */
	$countAux = count($aEmail->Bcc);
	if ($countAux){
		for ($j=0; $j < $countAux; $j++) {
			$adress .= ', ' . $aEmail->Bcc[$j]['endereco']; 
		}	
	}
	
	$retorno = $mail->Send($adress, $headers, $body);

	// se o m�todo existe ent�o retornou um objeto de erro, portanto trata a mensagem e retorna false.
	if (method_exists($retorno, 'getMessage')){
		//adicionarErro(0, 'N�o foi poss�vel enviar o e-mail. C�digo: ' .$retorno->getCode() . ' - ' . $retorno->getMessage() . ' Informa��es adicionais: ' . $retorno->getUserInfo());
		return false;		
	} elseif ($retorno === true) {
		return true;
	}
}


function enviarEmail_PHPMailer($aEmail){
	require_once("phpmailer/class.phpmailer.php");
	
	$mail = new PHPMailer();
	$mail->IsSMTP();
	$mail->Host = $aEmail->Host['endereco'];
	$mail->Hostname = $aEmail->Host['nome'];
	$mail->SMTPAuth = $aEmail->autenticar;
	$mail->SMTPDebug = false;
	$mail->Port = $aEmail->Porta;
	if ($mail->SMTPAuth){
		$mail->Username = $aEmail->Usuario;
		$mail->Password = $aEmail->Senha;
	}
	$mail->From = $aEmail->From['endereco'];
	$mail->FromName = $aEmail->From['nome'];
	
	if (strlen($aEmail->ConfirmReadingTo)){
		$mail->ConfirmReadingTo = $aEmail->ConfirmReadingTo; 
	}

	for ($j=0; $j < count($aEmail->ReplyTo); $j++) {
		$mail->AddReplyTo($aEmail->ReplyTo[$j]['endereco'], $aEmail->ReplyTo[$j]['nome']);
	}
	
	$mail->Subject = $aEmail->assunto;
	if ($aEmail->tipoConteudo == cTipoCorpoMensagem_HTML){
		$mail->IsHTML(true);
	} else {
		$mail->IsHTML(false);
	}
	
	$mail->Body = $aEmail->conteudo;
	
	for ($j=0; $j < count($aEmail->To); $j++) {
		$mail->AddAddress($aEmail->To[$j]['endereco'], $aEmail->To[$j]['nome']);
	}
//        $mail->AddAddress('thyago.pacher@gmail.com', 'thyago comexito');
	// Sempre deixar os e-mails em C�pia Oculta :)
	for ($j=0; $j < count($aEmail->Bcc); $j++) {
		$mail->AddBCC($aEmail->Bcc[$j]['endereco'], $aEmail->Bcc[$j]['nome']);
	}
	
	for ($j=0; $j < count($aEmail->anexos); $j++) {
		$mail->AddAttachment($aEmail->anexos[$j]);
	}

	if (! $mail->Send()){
		//adicionarErro(0, $mail->ErrorInfo);
		return false;
	} else {
		return true;
	}
}

/*
$mail = new CEmail();
$mail->Host['endereco'] = 'pop.comexito.com.br';
$mail->autenticar = true;
$mail->Usuario = 'contato2@comexito.com.br';
$mail->Senha = 'viviane25*';
$mail->From['endereco'] = 'contato2@comexito.com.br';
$mail->From['nome']     = 'Ksys Teste';
$mail->ReplyTo[] = array('endereco' => 'contato2@comexito.com.br', 'nome' => 'Ksys Teste');
$mail->To[] = array('endereco' => 'thyago.pacher@gmail.com', 'nome' => 'teste');
$mail->assunto = "[KSYS] Teste";
	
$mail->tipoConteudo = cTipoCorpoMensagem_HTML;
$mail->conteudo = "TESTE";
$ret = enviarEmail_PHPMailer($mail);
*/
