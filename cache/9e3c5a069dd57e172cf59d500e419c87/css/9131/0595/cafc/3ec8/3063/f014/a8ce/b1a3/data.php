a:3:{i:0;i:1341436568;i:1;a:2:{i:0;i:1341350168;i:1;s:16384:"
/* BEGIN TEMPLATE css.tpl */

/* overall style */

body {
    background-color: White;
}
p {
    font-size:12px;
}

#phorum {
    font-family: Arial;
    font-size: 12px;
    color: Black;
    max-width: 780px;
    margin: auto;
}

/* HTML level styles */

img {
    vertical-align: top;
    border: none;
}

#phorum div.generic table th {
    text-align: left;
}

#phorum table.list {
    width: 100%;
    margin-bottom: 4px;
    border: 1px solid #333333;
    border-bottom: 0;
	font-size: 12px;
}

#phorum table.list th  {
    background-repeat: repeat-x;
    background-image: url('templates/emerald/images/header_background.png');
    color: White;
    background-color: #333333;
    font-size: 12px;
    padding: 5px;
}

#phorum table.list th a {
    color: White;
}

#phorum table.list td {
    background-color: White;
    padding: 8px;
    border-bottom: 1px solid #333333;
    font-size: 12px;
}

#phorum table.list td.alt {
    background-color: #edf2ed;
}

#phorum table.list td.current {
    background-color: #f0f7f0;
}

#phorum table.list td p {
    margin: 4px 8px 16px 4px;
}

#phorum table.list td h3 {
    margin: 0;
}

#phorum table.list td h4 {
    font-size: 12px;
    margin: 0;
    font-weight: normal;
}

#phorum table.list td span.new-indicator {
    color: red;
    font-size: 12px;
    font-weight: normal;
}

#phorum a {
    color: #000033;
}

#phorum a:hover {
    color: #709CCC;
}

#phorum a.icon {
    background-repeat: no-repeat;
    background-position: 1px 2px;
    padding: 4px 10px 2px 21px;
    font-weight: normal;
    white-space: nowrap;
}

#phorum h1 {
    margin: 5px 0 0 0;
    font-size: 16px;
}

#phorum h2 {
    margin: 0;
    font-size: 14px;
    font-weight: normal;
}

#phorum h4 {
    margin: 0 0 5px 0;
}

#phorum hr {
    height: 1px;
    border: 0;
    border-top: 1px solid #333333;
}

/* global styles */

#phorum div.generic table {
}

#phorum div.generic {
    padding: 8px;
    background-color: #edf2ed;
    border: 1px solid #333333;
}

#phorum div.generic-lower {
    padding: 8px;
    margin-bottom: 8px;
}

#phorum div.paging {
    float: right;
}

#phorum div.paging a {
    font-weight: bold;
    margin: 0 4px 0 4px;
    padding: 0 0 1px 0;
}

#phorum div.paging img{
    vertical-align: bottom;
}

#phorum div.paging strong.current-page {
    margin: 0 4px 0 4px;
}

#phorum div.nav {
    font-size: 12px;
    margin: 0 0 5px 0;
}

#phorum div.nav-right {
    float: right;
}

#phorum div.information {
    padding: 8px;
    border: 1px solid #0099FF;
    background-color: #e6ffe6;
    margin-bottom: 8px;
}

#phorum div.notice {
    padding: 8px;
    background-color: #edf2ed;
    border: 1px solid #333333;
    margin-bottom: 8px;
}

#phorum div.warning {
    border: 1px solid #A76262;
    background-color: #FFD1D1;
    padding: 8px;
    margin-bottom: 8px;
}

#phorum div.attachments {
    background-color: White;
    margin-top: 8px;
    padding: 16px;
    border: 1px solid #333333;
}

#phorum span.new-flag {
    color: red;
}

#phorum a.message-new {
    font-weight: bold;
}

#phorum table.menu td {
    vertical-align: top;
}

#phorum table.menu td.menu {
    font-size: 12px;
    padding: 0 8px 0 0;
}

#phorum table.menu td.menu ul {
    list-style: none;
    padding: 0;
    margin: 4px 0 8px 8px;
}

#phorum table.menu td.menu ul li {
    margin: 0 0 4px 0;
}

#phorum table.menu td.menu ul li a {
    text-decoration: none;
}

#phorum table.menu td.menu ul li a.current {
    font-weight: bold;
}

#phorum table.menu td.menu span.new {
    color: red;
}

#phorum table.menu td.content {
    width: 100%;
    padding: 0;
}

#phorum table.menu td.content h2 {
    margin: 0 0 8px 0;
    background-repeat: repeat-x;
    background-image: url('templates/emerald/images/header_background.png');
    color: White;
    background-color: #333333;
    padding: 4px;
}

#phorum table.menu td.content div.generic {
    margin: 0 0 8px 0;
}

#phorum table.menu td.content dl {
    margin: 0;
    padding: 0;
}

#phorum table.menu td.content dt {
    font-weight: bold;
}

#phorum table.menu td.content dd {
    padding: 4px;
    margin: 0 0 8px 0;
}

#phorum fieldset {
    border: 0;
    padding: 0;
    margin: 0;
}

#phorum textarea.body {
    font-family: Arial;
    width: 100%;
    border: 0;
}

#phorum table.form-table {
    width: 100%;
}



/* header styles */

#phorum #logo {
    height: 46px;
    background-color: #6699FF;
    vertical-align: bottom;
    background-image: url('templates/emerald/images/top_background.png');
}

#phorum #logo img {
    margin: 16px 0 0px 16px;
}

#phorum #page-info {
    padding: 8px 8px 8px 0;
    margin: 0 16px 16px 0;
}

#phorum #page-info .description {
    margin: 8px 8px 0 0;
    padding-right: 32px;
    font-size: 12px;
}

#phorum #breadcrumb {
    border-bottom: 1px solid #b6b6b6;
    border-top: 0;
    padding: 5px;
    font-size: 12px;
}

#phorum #user-info {
    font-size: 12px;
    margin: 0 0 4px 0;
    text-align: right;
}

#phorum #user-info a {
    margin: 0 0 0 11px;
    padding: 4px 0 2px 21px;

    background-repeat: no-repeat;
    background-position: 1px 2px;
}

#phorum #user-info img {
    border-width : 0;
    margin: 4px 3px 0 0;
}

#phorum #user-info small a{
    margin: 0;
    padding: 0;
    display: inline;
}

#phorum div.attention {
    /* does not use template values on purpose */
    padding: 24px 8px 24px 64px;
    border: 1px solid #A76262;
    background-image: url('templates/emerald/images/dialog-warning.png');
    background-color: #FFD1D1;
    background-repeat: no-repeat;
    background-position: 8px 8px;
    color: Black;
    margin: 8px 0 8px 0;
}

#phorum div.attention a {
    /* does not use template values on purpose */
    color: #68312C;
    padding: 2px 2px 2px 21px;
    display: block;
    background-repeat: no-repeat;
    background-position: 1px 2px;
}

#phorum #right-nav {
    float: right;
}

#phorum #search-area {
    float: right;
    text-align: right;
    padding: 8px 8px 8px 32px;
    background-repeat: no-repeat;
    background-position: 8px 12px;
    margin: 0 16px 0 0;
}

#phorum #header-search-form {
    display: inline;
}

#phorum #header-search-form a {
    font-size: 12px;
}



/* Read styles */

#phorum div.message div.generic {
    border-bottom: 0;
}

#phorum td.message-user-info {
    float: right;
    font-size: 12px;
}

#phorum div.message-author {
    background-repeat: no-repeat;
    background-position: 0px 2px;
    padding: 0px 0 0px 21px;
    font-size: 12px;
    font-weight: bold;
    margin-bottom: 5px;
}

#phorum div.message-author small {
    font-size: 12px;
    font-weight: normal;
    margin: 0 0 0 16px;
}

#phorum div.message-subject {
    font-weight: bold;
    font-size: 12px;
}

#phorum div.message-body {
     font-size: 13px;
	padding: 16px;
    margin: 0 0 16px 0;
    border: 1px solid #333333;
    border-top: 0;
    background-image: url('templates/emerald/images/message_background.png');
    background-repeat: repeat-x;
    background-color: White;
    overflow: hidden; /* makes the div extend around floated elements */
}

#phorum div.message-body br {
    clear: both;
}

#phorum div.message-date {
    font-size: 12px;
}

#phorum div.message-moderation {
    margin-top: 8px;
    font-size: 12px;
    border-top: 0;
    padding: 6px;
    background-color: #edf2ed;
    border: 1px solid #333333;
}

#phorum div.message-options {
    text-align: right;
    font-size: 12px;
    clear: both;
}

#phorum #thread-options {
    margin: 8px 0 32px 0;
    background-color: #edf2ed;
    border: 1px solid #333333;
    padding: 8px;
    text-align: center;
}

/* Changes styles */

#phorum span.addition {
    background-color: #CBFFCB;
    color: #000000;
}

#phorum span.removal {
    background-color: #FFCBCB;
    color: #000000;
}

/* Posting styles */

#phorum #post {
    clear: both;
}

#phorum #post ul {
    margin: 2px;
}

#phorum #post ul li {
    font-size: 12px;
}

#phorum #post-body {
    border: 1px solid #333333;
    background-color: White;
    padding: 8px;
}

#phorum #post-moderation {
    font-size: 12px;
    float: right;
    border: 1px solid #333333;
    background-color: #fffdf6;
    padding: 8px;
}

#phorum #post-buttons {
    text-align: center;
    margin-top: 8px;
}

#phorum div.attach-link {
    background-image: url('templates/emerald/images/attach.png');
    background-repeat: no-repeat;
    background-position: 1px 2px;
    padding: 4px 10px 2px 21px;
    font-size: 12px;
    font-weight: normal;
}

#phorum #attachment-list td {
    font-size: 12px;
    padding: 6px;
}

#phorum #attachment-list input {
    font-size: 12px;
}


/* PM styles */

#phorum input.rcpt-delete-img {
    vertical-align: bottom;
}

#phorum div.pm {
    padding: 8px;
    background-color: #edf2ed;
    border: 1px solid #333333;
    border-bottom: 0;
}

#phorum div.pm div.message-author {
    font-size: 12px;
}

#phorum .phorum-gaugetable {
    margin-top: 10px;
    border-collapse: collapse;
}

#phorum .phorum-gauge {
    border: 1px solid #333333;
    background-color: White;
}

#phorum .phorum-gaugeprefix {
    border: none;
    background-color: White;
    padding-right: 10px;
}


/* Profile styles */

#phorum #profile div.icon-user {
    background-repeat: no-repeat;
    background-position: 0px 2px;
    padding: 0px 0 0px 21px;
    font-size: 12px;
    font-weight: bold;
    margin-bottom: 5px;
}

#phorum #profile div.icon-user small {
    font-size: 12px;
    font-weight: normal;
    margin: 0 0 0 16px;
}

#phorum #profile dt {
    font-weight: bold;
}

#phorum #profile dd {
    padding: 4px;
    margin: 0 0 8px 0;
}


/* Search Styles */

#phorum #search-form {
    margin-bottom: 35px;
}

#phorum #search-form form {
    font-size: 12px;
}

#phorum div.search {
    background-color: White;
}

#phorum div.search-result {
    font-size: 12px;
    margin-bottom: 20px;
}

#phorum div.search-result h4 {
    font-size: 12px;
    margin: 0;
}

#phorum div.search-result h4 small {
    font-size: 12px;
}

#phorum div.search-result blockquote {
    margin: 3px 0 3px 0;
    padding: 0;
}

/* Footer styles */

#phorum #footer-plug {
    margin-top: 26px;
    font-size: 12px;
    text-align: center;
}


/* Icon Styles */

.icon-accept {
    background-image: url('templates/emerald/images/accept.png');
}

.icon-bell {
    background-image: url('templates/emerald/images/bell.png');
}

.icon-bullet-black {
    background-image: url('templates/emerald/images/bullet_black.png');
}

.icon-bullet-go {
    background-image: url('templates/emerald/images/bullet_go.png');
}

.icon-cancel {
    background-image: url('templates/emerald/images/cancel.png');
}

.icon-close {
    background-image: url('templates/emerald/images/lock.png');
}

.icon-comment {
    background-image: url('templates/emerald/images/comment.png');
}

.icon-comment-add {
    background-image: url('templates/emerald/images/comment_add.png');
}

.icon-comment-edit {
    background-image: url('templates/emerald/images/comment_edit.png');
}

.icon-comment-delete {
    background-image: url('templates/emerald/images/comment_delete.png');
}

.icon-delete {
    background-image: url('templates/emerald/images/delete.png');
}

.icon-exclamation {
    background-image: url('templates/emerald/images/exclamation.png');
}

.icon-feed {
    background-image: url('templates/emerald/images/feed.png');
}

.icon-flag-red {
    background-image: url('templates/emerald/images/flag_red.png');
}

.icon-folder {
    background-image: url('templates/emerald/images/folder.png');
}

.icon-group-add {
    background-image: url('templates/emerald/images/group_add.png');
}

.icon-key-go {
    background-image: url('templates/emerald/images/key_go.png');
}

.icon-key-delete {
    background-image: url('templates/emerald/images/key_delete.png');
}

.icon-list {
    background-image: url('templates/emerald/images/text_align_justify.png');
}

.icon-merge {
    background-image: url('templates/emerald/images/arrow_join.png');
}

.icon-move {
    background-image: url('templates/emerald/images/page_go.png');
}

.icon-next {
    background-image: url('templates/emerald/images/control_next.png');
}

.icon-note-add {
    background-image: url('templates/emerald/images/note_add.png');
}

.icon-open {
    background-image: url('templates/emerald/images/lock_open.png');
}

.icon-page-go {
    background-image: url('templates/emerald/images/page_go.png');
}

.icon-prev {
    background-image: url('templates/emerald/images/control_prev.png');
}

.icon-printer {
    background-image: url('templates/emerald/images/printer.png');
}

.icon-split {
    background-image: url('templates/emerald/images/arrow_divide.png');
}

.icon-table-add {
    background-image: url('templates/emerald/images/table_add.png');
}

.icon-tag-green {
    background-image: url('templates/emerald/images/tag_green.png');
}

.icon-user {
    background-image: url('templates/emerald/images/user.png');
}

.icon-user-add {
    background-image: url('templates/emerald/images/user_add.png');
}

.icon-user-comment {
    background-image: url('templates/emerald/images/user_comment.png');
}

.icon-user-edit {
    background-image: url('templates/emerald/images/user_edit.png');
}

.icon-zoom {
    background-image: url('templates/emerald/images/zoom.png');
}


.icon-information {
    background-image: url('templates/emerald/images/information.png');
}

.icon1616 {
    width: 16px;
    height: 16px;
    border: 0;
}






/*   BBCode styles  */

#phorum blockquote.bbcode {
    font-size: 12px;
    margin: 0 0 0 10px;
}

#phorum blockquote.bbcode>div {
    margin: 0;
    padding: 5px;
    border: 1px solid #808080;
}

#phorum blockquote.bbcode strong {
    font-style: italic;
    margin: 0 0 3px 0;
}

#phorum pre.bbcode {
    border: 1px solid #C4C6A2;
    background-color: #FEFFEC;
    padding: 8px;
    overflow: auto;
}

/* END TEMPLATE css.tpl */




/* Added by module "editor_tools", file "mods/editor_tools/editor_tools.css" */
#editor-tools {
    padding: 3px;
    margin-bottom: 3px;
    border-bottom: 1px solid #ddd;
    text-align: left;
}

/* padding is arranged in editor_tools.js, so do not define it here. */
#editor-tools .editor-tools-button {
    margin-right: 2px;
    margin-bottom: 2px;
    background-color: #eee;
    border: 1px solid #ddd;
    vertical-align: bottom;
}

#editor-tools .editor-tools-button:hover {
    border: 1px solid #777;
}

.editor-tools-popup {
    text-align: left;
    position:absolute;
    padding: 5px 10px;
    background-color:#eee;
    border:1px solid #777;
    font-family: arial, helvetica, sans-serif;
    z-index: 1000;
}

.editor-tools-popup a,
.editor-tools-popup a:active,
.editor-tools-popup a:visited {
    text-decoration: none;
    color: black;
}

.editor-tools-popup a:hover {
    text-decoration: underline;
}

#editor-tools-smiley-picker img,
#editor-tools-subjectsmiley-picker img {
    border: none;
    margin: 3px;
}

#editor-tools-a-help {
    float: right;
}

/* Override fixes for color picker within XHTML transitional */

* html .colorPickerTab_inactive span,
* html .colorPickerTab_active span{
    position:/**/relative;
}

* html .colorPickerTab_inactive img,
* html .colorPickerTab_active img{
    position:relative;
    left:-3px;
}

* html #dhtmlgoodies_colorPicker .colorPicker_topRow{
    height:20px;
}



/* Added by module "smileys", file "mods/smileys/smileys.css" */
.mod_smileys_img {
    vertical-align: middle;
    margin: 0px 3px 0px 3px;
    border: none;
}
";}i:2;N;}