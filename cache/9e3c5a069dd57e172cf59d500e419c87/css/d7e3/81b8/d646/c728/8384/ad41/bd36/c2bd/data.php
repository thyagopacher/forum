a:3:{i:0;i:1339190769;i:1;a:2:{i:0;i:1339104369;i:1;s:15212:"
/* Element level classes */

body
{
    color: Black;
    font-size: 12px;
    font-family: "Bitstream Vera Sans", "Lucida Sans Unicode", "Lucida Grande", Arial;
    background-color: White;
    margin: 8px;
}

td, th
{
    color: Black;
    font-size: 12px;
    font-family: "Bitstream Vera Sans", "Lucida Sans Unicode", "Lucida Grande", Arial;
}

img
{
    border-width: 0px;
    vertical-align: middle;
}

a
{
    color: #000099;
    text-decoration: none;
}
a:active
{
    color: #FF6600;
    text-decoration: none;
}
a:visited
{
    color: #000099;
    text-decoration: none;
}

a:hover
{
    color: #FF6600;
}

input[type=text], input[type=password], input[type=file], select
{
    background-color: White;
    color: Black;
    font-size: 12px;
    font-family: "Bitstream Vera Sans", "Lucida Sans Unicode", "Lucida Grande", Arial;

    vertical-align: middle;

}

textarea
{
    background-color: White;
    color: Black;
    font-size: 12px;
    font-family: Lucida Console, Andale Mono, Courier New, Courier;
}

input[type=submit]
{
    border: 1px dotted #808080;
    background-color: #EEEEEE;
    font-size: 12px;
    font-family: "Bitstream Vera Sans", "Lucida Sans Unicode", "Lucida Grande", Arial;
    vertical-align: middle;
}

input
{
    vertical-align: middle;
}


/* new styles */

#phorum-index
{
    width: 100%;
    border: 1px solid #808080;
    border-bottom-width: 0px;
    text-align: left;
}

#phorum-index th
{
    border-bottom: 1px solid #808080;
    background-color: #EEEEEE;
    padding: 3px 0 3px 0;
}

#phorum-index td
{
    font-family: "Bitstream Vera Sans", "Trebuchet MS", Verdana, Arial, sans-serif;
    background-color: White;
    padding: 3px 0 3px 0;
    border-bottom: 1px solid #808080;
}

#phorum-index th.forum-name
{
    font-family: "Bitstream Vera Sans", "Trebuchet MS", Verdana, Arial, sans-serif;
    font-size: 16px;
    padding: 3px 0 3px 3px;
}

#phorum-index th.forum-name a
{
    color: Black;
}

#phorum-index th.forum-threads
{
    width: 120px;
    text-align: center;
    vertical-align: middle;
}

#phorum-index th.forum-posts
{
    width: 120px;
    text-align: center;
    vertical-align: middle;
}

#phorum-index th.forum-last-post
{
    padding: 3px 15px 3px 3px;
    vertical-align: middle;
}

#phorum-index td.forum-name
{
    font-family: "Bitstream Vera Sans", "Trebuchet MS", Verdana, Arial, sans-serif;
    font-size: 13px;
    font-weight: bold;
    padding: 5px 0 5px 15px;
}

#phorum-index td.forum-name p
{
    font-size: 13px;
    font-weight: normal;
    font-family: "Bitstream Vera Sans", "Lucida Sans Unicode", "Lucida Grande", Arial;
    margin: 0 15px 0 0;
}

#phorum-index td.forum-name small
{
    font-weight: normal;
    font-family: "Bitstream Vera Sans", "Lucida Sans Unicode", "Lucida Grande", Arial;
}

#phorum-index td.forum-threads
{
    width: 120px;
    text-align: center;
}

#phorum-index td.forum-posts
{
    width: 120px;
    text-align: center;
}

#phorum-index td.forum-last-post
{
    width: 120px;
    padding: 0 15px 0 0;
}

#phorum-menu-table
{
    width: 100%;
    border-width: 0px;
}

#phorum-menu
{
    padding: 5px 3px 0 0;
    vertical-align: top;
    width: 200px;
}

#phorum-content
{
    padding: 5px 0 0 2px;
    vertical-align: top;
}

div.phorum-menu
{
    font-size: 12px;
    font-family: "Bitstream Vera Sans", "Lucida Sans Unicode", "Lucida Grande", Arial;
    background-color: White;
    border: 1px solid #808080;
    padding: 3px;
}

div.phorum-menu ul
{
    font-weight: bold;
    list-style: none;
    padding: 0;
    margin: 0 0 10px 0;
}

div.phorum-menu li
{
    font-weight: bold;
    font-family: Lucida Sans Unicode, Lucida Grande, Arial;
    font-size: 12px;
    padding: 0 0 0 15px;
    margin-top:3px;
    background-image: url('templates/classic/images/square_bullet.png');
    background-repeat: no-repeat;
    background-position: 1px 2px;
}

div.phorum-menu a
{
    font-weight: normal;
    color: #000000;
}

div.phorum-menu a:hover
{
    color: #FF6600;
}

div.phorum-menu a.phorum-current-page
{
    font-weight: bold;
}

#phorum-post-form ul
{
    padding: 0 0 0 20px;
    margin: 3px 0px 8px 0px;
    font-size: 11px;
}

#phorum-post-form li
{
    margin-bottom: 3px;
}

#phorum-attachment-list td
{
    font-size: 11px;
}

    /* Standard classes for use in any page */
    /* PhorumDesignDiv - a div for keeping the forum-size size */
    .PDDiv
    {
        width: 100%;
        text-align: left;
    }
    /* new class for layouting the submit-buttons in IE too */
    .PhorumSubmit {
        border: 1px dotted #808080;
        color: Black;
        background-color: #EEEEEE;
        font-size: 12px;
        font-family: "Bitstream Vera Sans", "Lucida Sans Unicode", "Lucida Grande", Arial;
        vertical-align: middle;
    }

    .PhorumTitleText
    {
        float: right;
    }

    .PhorumStdBlock
    {
        font-size: 12px;
        font-family: "Bitstream Vera Sans", "Lucida Sans Unicode", "Lucida Grande", Arial;
        background-color: White;
        border: 1px solid #808080;
/*        width: 100%; */
        padding: 3px;
        text-align: left;
    }

    .PhorumStdBlockHeader
    {
        font-size: 12px;
        font-family: "Bitstream Vera Sans", "Lucida Sans Unicode", "Lucida Grande", Arial;
        background-color: #EEEEEE;
/*        width: 100%; */
        border-left: 1px solid #808080;
        border-right: 1px solid #808080;
        border-top: 1px solid #808080;
        padding: 3px;
        text-align: left;
    }

    .PhorumHeaderText
    {
        font-weight: bold;
    }

    .PhorumNavBlock
    {
        font-size: 12px;
        font-family: Lucida Sans Unicode, Lucida Grande, Arial;
        border: 1px solid #808080;
        margin-top: 1px;
        margin-bottom: 1px;
/*        width: 100%; */
        background-color: #EEEEEE;
        padding: 2px 3px 2px 3px;
    }

    .PhorumNavHeading
    {
        font-weight: bold;
    }

    A.PhorumNavLink
    {
        color: #000000;
        text-decoration: none;
        font-weight: normal;
        font-family: Lucida Sans Unicode, Lucida Grande, Arial;
        font-size: 12px;
        border-style: solid;
        border-color: #EEEEEE;
        border-width: 1px;
        padding: 0px 4px 0px 4px;
    }

    .PhorumSelectedFolder
    {
        color: #000000;
        text-decoration: none;
        font-weight: normal;
        font-family: Lucida Sans Unicode, Lucida Grande, Arial;
        font-size: 12px;
        border-style: solid;
        border-color: #EEEEEE;
        border-width: 1px;
        padding: 0px 4px 0px 4px;
    }

    A.PhorumNavLink:hover
    {
        background-color: #FFFFFF;
        font-weight: normal;
        font-family: Lucida Sans Unicode, Lucida Grande, Arial;
        font-size: 12px;
        border-style: solid;
        border-color: #808080;
        border-width: 1px;
        color: #FF6600;
    }

    .PhorumFloatingText
    {
        padding: 10px;
    }

    .PhorumHeadingLeft
    {
        padding-left: 3px;
        font-weight: bold;
    }

    .PhorumUserError
    {
        padding: 10px;
        text-align: center;
        color: Red;
        font-size: 16px;
        font-family: "Bitstream Vera Sans", "Trebuchet MS", Verdana, Arial, sans-serif;
        font-weight: bold;
    }

    .PhorumOkMsg
    {
        padding: 10px;
        text-align: center;
        color: DarkGreen;
        font-size: 16px;
        font-family: "Bitstream Vera Sans", "Trebuchet MS", Verdana, Arial, sans-serif;
        font-weight: bold;
    }

   .PhorumNewFlag
    {
        font-family: "Bitstream Vera Sans", "Lucida Sans Unicode", "Lucida Grande", Arial;
        font-size: 10px;
        font-weight: bold;
        color: #CC0000;
    }

    .PhorumNotificationArea
    {
        float: right;
        border-style: dotted;
        border-color: #808080;
        border-width: 1px;
    }

    /* PSUEDO Table classes                                       */
    /* In addition to these, each file that uses them will have a */
    /* column with a style property to set its right margin       */

    .PhorumColumnFloatXSmall
    {
        float: right;
        width: 75px;
    }

    .PhorumColumnFloatSmall
    {
        float: right;
        width: 100px;
    }

    .PhorumColumnFloatMedium
    {
        float: right;
        width: 150px;
    }

    .PhorumColumnFloatLarge
    {
        float: right;
        width: 200px;
    }

    .PhorumColumnFloatXLarge
    {
        float: right;
        width: 400px;
    }

    .PhorumRowBlock
    {
        background-color: White;
        border-bottom: 1px solid #F2F2F2;
        padding: 5px 0px 0px 0px;
    }

    .PhorumRowBlockAlt
    {
        background-color: #EEEEEE;
        border-bottom: 1px solid #F2F2F2;
        padding: 5px 0px 0px 0px;
    }

    /************/


    /* All that is left of the tables */

    .PhorumStdTable
    {
        border-style: solid;
        border-color: #808080;
        border-width: 1px;
        width: 100%;
    }

    .PhorumTableHeader
    {
        background-color: #EEEEEE;
        border-bottom-style: solid;
        border-bottom-color: #808080;
        border-bottom-width: 1px;
        color: #000000;
        font-size: 12px;
        font-family: Lucida Sans Unicode, Lucida Grande, Arial;
        font-weight: bold;
        padding: 3px;
    }

    .PhorumTableRow
    {
        background-color: White;
        border-bottom-style: solid;
        border-bottom-color: #F2F2F2;
        border-bottom-width: 1px;
        color: Black;
        font-size: 12px;
        font-family: "Bitstream Vera Sans", "Lucida Sans Unicode", "Lucida Grande", Arial;
        height: 35px;
        padding: 3px;
        vertical-align: middle;
    }

    .PhorumTableRowAlt
    {
        background-color: #EEEEEE;
        border-bottom-style: solid;
        border-bottom-color: #F2F2F2;
        border-bottom-width: 1px;
        color: #000000;
        font-size: 12px;
        font-family: "Bitstream Vera Sans", "Lucida Sans Unicode", "Lucida Grande", Arial;
        height: 35px;
        padding: 3px;
        vertical-align: middle;
    }

    table.PhorumFormTable td
    {
        height: 26px;
    }

    /**********************/


    /* Read Page specifics */

    .PhorumReadMessageBlock
    {
        margin-bottom: 5px;
    }

   .PhorumReadBodySubject
    {
        color: Black;
        font-size: 16px;
        font-family: "Bitstream Vera Sans", "Trebuchet MS", Verdana, Arial, sans-serif;
        font-weight: bold;
        padding-left: 3px;
    }

    .PhorumReadBodyHead
    {
        padding-left: 5px;
    }

    .PhorumReadBodyText
    {
        font-size: 12px;
        font-family: "Bitstream Vera Sans", "Lucida Sans Unicode", "Lucida Grande", Arial;
        padding: 5px;
    }

    .PhorumReadNavBlock
    {
        font-size: 12px;
        font-family: Lucida Sans Unicode, Lucida Grande, Arial;
        border-left: 1px solid #808080;
        border-right: 1px solid #808080;
        border-bottom: 1px solid #808080;
/*        width: 100%; */
        background-color: #EEEEEE;
        padding: 2px 3px 2px 3px;
    }

    /********************/

    /* List page specifics */

    .PhorumListSubText
    {
        color: #707070;
        font-size: 10px;
        font-family: "Bitstream Vera Sans", Arial, sans-serif;
    }

    .PhorumListPageLink
    {
        color: #707070;
        font-size: 10px;
        font-family: "Bitstream Vera Sans", Arial, sans-serif;
    }

    .PhorumListSubjPrefix
    {
        font-weight: bold;
    }

    /********************/

    /* Posting editor specifics */

    .PhorumListModLink, .PhorumListModLink a
    {
        color: #707070;
        font-size: 10px;
        font-family: "Bitstream Vera Sans", Arial, sans-serif;
    }

    .PhorumAttachmentRow {
        border-bottom: 1px solid #EEEEEE;
        padding: 3px 0px 3px 0px;
    }

    /********************/

    /* PM specifics */

    .phorum-recipientblock
    {
        border: 1px solid black;
        position:relative;
        float:left;
        padding: 1px 1px 1px 5px;
        margin: 0px 5px 5px 0px;
        font-size: 11px;
        background-color: White;
        border: 1px solid #808080;
        white-space: nowrap;
    }

    .phorum-pmuserselection
    {
        padding-bottom: 5px;
    }

    .phorum-gaugetable {
        border-collapse: collapse;
    }

    .phorum-gauge {
        border: 1px solid #808080;
        background-color: #EEEEEE;
    }

    .phorum-gaugeprefix {
        border: none;
        background-color: white;
        padding-right: 10px;
    }

    /********************/

    /* Override classes - Must stay at the end */

    .PhorumNarrowBlock
    {
        width: 600px;
    }

    .PhorumSmallFont
    {
        font-size: 11px;
    }

    .PhorumLargeFont
    {
        color: Black;
        font-size: 16px;
        font-family: "Bitstream Vera Sans", "Trebuchet MS", Verdana, Arial, sans-serif;
        font-weight: bold;
    }


    .PhorumFooterPlug
    {
        margin-top: 10px;
        font-size: 10px;
        font-family: "Bitstream Vera Sans", Arial, sans-serif;
    }



    /*   BBCode styles  */

    blockquote.bbcode
    {
        font-size: 11px;
        margin: 0 0 0 10px;
    }

    blockquote.bbcode>div
    {
        margin: 0;
        padding: 5px;
        border: 1px solid #808080;
        overflow: hidden;
    }

    blockquote.bbcode strong
    {
        font-style: italic;
        margin: 0 0 3px 0;
    }




/* Added by module "editor_tools", file "mods/editor_tools/editor_tools.css" */
#editor-tools {
    padding: 3px;
    margin-bottom: 3px;
    border-bottom: 1px solid #ddd;
    text-align: left;
}

/* padding is arranged in editor_tools.js, so do not define it here. */
#editor-tools .editor-tools-button {
    margin-right: 2px;
    margin-bottom: 2px;
    background-color: #eee;
    border: 1px solid #ddd;
    vertical-align: bottom;
}

#editor-tools .editor-tools-button:hover {
    border: 1px solid #777;
}

.editor-tools-popup {
    text-align: left;
    position:absolute;
    padding: 5px 10px;
    background-color:#eee;
    border:1px solid #777;
    font-family: arial, helvetica, sans-serif;
    z-index: 1000;
}

.editor-tools-popup a,
.editor-tools-popup a:active,
.editor-tools-popup a:visited {
    text-decoration: none;
    color: black;
}

.editor-tools-popup a:hover {
    text-decoration: underline;
}

#editor-tools-smiley-picker img,
#editor-tools-subjectsmiley-picker img {
    border: none;
    margin: 3px;
}

#editor-tools-a-help {
    float: right;
}

/* Override fixes for color picker within XHTML transitional */

* html .colorPickerTab_inactive span,
* html .colorPickerTab_active span{
    position:/**/relative;
}

* html .colorPickerTab_inactive img,
* html .colorPickerTab_active img{
    position:relative;
    left:-3px;
}

* html #dhtmlgoodies_colorPicker .colorPicker_topRow{
    height:20px;
}



/* Added by module "smileys", file "mods/smileys/smileys.css" */
.mod_smileys_img {
    vertical-align: middle;
    margin: 0px 3px 0px 3px;
    border: none;
}
";}i:2;N;}